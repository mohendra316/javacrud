package controller;

import Dao.DataAccessObject;
import service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by mohit on 3/21/18.
 */
@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String userName, password;
        userName = request.getParameter("userName");
        password = request.getParameter("password");

        LoginService loginService = new LoginService();

        DataAccessObject dataAccessObject = new DataAccessObject();
        dataAccessObject.setUserName(userName);
        dataAccessObject.setPassword(password);

        boolean result = loginService.authenticate(dataAccessObject);
        if(result){
            response.sendRedirect("success.jsp");
            return;
        }
        else{
            response.sendRedirect("index.jsp");
            return;
        }
    }

//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//
//    }
}
