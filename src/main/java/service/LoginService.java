package service;

import Dao.DataAccessObject;
import dbConnection.DBConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by mohit on 3/21/18.
 */
public class LoginService {
    public boolean authenticate(DataAccessObject dataAccessObject){
//        if(password == null || password.trim() == "" ){
//            return false;
//        }
//        return true;
        String username = dataAccessObject.getUserName();
        String password = dataAccessObject.getPassword();

        Connection con = null;
        Statement statement = null;
        ResultSet resultSet = null;

        String userNameDB ="";
        String passwordDB = "";
        //String roleDB = "";

        try{
            con = DBConnection.createConnection();
            statement = con.createStatement();
            resultSet =  statement.executeQuery("SELECT userName,password FROM tbl_users");

            while (resultSet.next()){
                userNameDB = resultSet.getString("userName");
                passwordDB = resultSet.getString("password");
                if(username.equals(userNameDB) && password.equals(passwordDB))
                    return true;
                else
                    return false;

            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return false;
    }
}
